use std::io::{self, BufRead};
mod cipher;
use cipher::affine::Affine;
use cipher::Cipher;

fn main() {
    println!("Enter a message to encrypt");
    let mut input = String::new();

    let stdin = io::stdin();
    stdin.lock().read_line(&mut input).unwrap();
    let crypto = Affine::new(1);
    let encrypted = crypto.encrypt(input.trim());
    let decrypted = crypto.decrypt(&encrypted);
    println!("Encrypted: {}", encrypted);
    println!("Decrypted: {}", decrypted);
}
