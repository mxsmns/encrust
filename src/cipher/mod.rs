pub mod affine;
pub trait Cipher {
    fn encrypt(&self, message: &str) -> String;
    fn decrypt(&self, message: &str) -> String;
}
