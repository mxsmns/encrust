use std::char;

pub struct Affine {
    shift: u32,
    size: u32,
}

impl Affine {
    pub fn new(shift: u32) -> Affine {
        Affine {
            shift: shift,
            size: 26,
        }
    }

    fn transform(&self, message: &str, shift: u32) -> String {
        let mut transformed = String::new();
        for c in message.chars() {
            transformed.push(self.shift_char(c, shift));
        }
        transformed
    }

    fn shift_char(&self, input: char, shift: u32) -> char {
        if !input.is_alphabetic() {
            return input;
        }
        let start = if input.is_ascii_lowercase() {
            'a' as u32
        } else {
            'A' as u32
        };
        let s = char::from_u32(((input as u32 + shift) - start) % self.size + start);
        match s {
            Some(shift) => shift,
            None => panic!("Couldn't convert character to u32"),
        }
    }
}

impl super::Cipher for Affine {
    fn encrypt(&self, message: &str) -> String {
        self.transform(message, self.shift)
    }

    fn decrypt(&self, message: &str) -> String {
        self.transform(message, (self.size - self.shift) % self.size)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::cipher::Cipher;
    #[test]
    fn test_affine() {
        let crypto = Affine::new(1);
        assert_eq!(crypto.encrypt("Hello, World #1!"), "Ifmmp, Xpsme #1!");
        assert_eq!(crypto.decrypt("Ifmmp, Xpsme #1!"), "Hello, World #1!");
    }
}
